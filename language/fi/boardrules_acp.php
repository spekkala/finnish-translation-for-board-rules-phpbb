<?php
/**
*
* Board Rules extension for the phpBB Forum Software package.
* Finnish translation by Sami Pekkala (https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb)
*
* @copyright (c) 2014 phpBB Limited <https://www.phpbb.com>
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(
	// Settings page
	'ACP_BOARDRULES'						=> 'Keskustelupalstan säännöt',
	'ACP_BOARDRULES_SETTINGS'				=> 'Sääntöjen asetukset',
	'ACP_BOARDRULES_SETTINGS_EXPLAIN'		=> 'Tässä voit muokata keskustelupalstan sääntöjen yleisiä asetuksia.',
	'ACP_BOARDRULES_ENABLE'					=> 'Keskustelupalstan säännöt käytössä',
	'ACP_BOARDRULES_HEADER_LINK'			=> 'Näytä linkki sääntöihin yläpalkissa',
	'ACP_BOARDRULES_FONT_ICON'				=> 'Sääntöjen linkin kuvake',
	'ACP_BOARDRULES_FONT_ICON_EXPLAIN'		=> 'Syötä yläpalkissa näytettävän keskustelupalstan sääntöjen linkin <a href="https://fortawesome.github.io/Font-Awesome/icons/" target="_blank">Font Awesome</a> -kuvakkeen nimi. Jätä kenttä tyhjäksi, jos et halua valita kuvaketta.',
	'ACP_BOARDRULES_FONT_ICON_INVALID'		=> 'Keskustelupalstan sääntöjen linkin kuvakkeen nimi sisälsi kelpaamattomia merkkejä.',
	'ACP_BOARDRULES_AT_REGISTRATION'		=> 'Vaadi uusia käyttäjiä hyväksymään säännöt rekisteröitymisen yhteydessä',
	'ACP_BOARDRULES_AT_REGISTRATION_EXPLAIN'=> 'Lisää käyttöehtoihin kohta, jossa vaaditaan uusia käyttäjiä lukemaan ja hyväksymään keskustelupalstan säännöt rekisteröitymisen yhteydessä.',
	'ACP_BOARDRULES_NOTIFY'					=> 'Ilmoita käyttäjille',
	'ACP_BOARDRULES_NOTIFY_EXPLAIN'			=> 'Lähetä kaikille rekisteröityneille käyttäjille ilmoitus keskustelupalstan sääntöjen muuttumisesta. (Tämä saattaa kestää useita sekunteja keskustelupalstoilla, joilla on tuhansia käyttäjiä.)',
	'ACP_BOARDRULES_NOTIFY_CONFIRM'			=> 'Haluatko varmasti lähettää ilmoituksen kaikille käyttäjille?',
	'ACP_BOARDRULES_SETTINGS_CHANGED'		=> 'Keskustelupalstan sääntöjen asetukset on päivitetty.',

	// Manage page
	'ACP_BOARDRULES_MANAGE'					=> 'Hallitse sääntöjä',
	'ACP_BOARDRULES_MANAGE_EXPLAIN'			=> 'Tässä voit luoda, muokata, poistaa ja järjestää kategorioita ja sääntöjä. Kategoria on toisiinsa liittyvien sääntöjen ryhmä. Jokainen kategoria voi sisältää rajattoman määrän sääntöjä.',
	'ACP_BOARDRULES_CATEGORY'				=> 'Sääntökategoria',
	'ACP_BOARDRULES_RULE'					=> 'Sääntö',
	'ACP_BOARDRULES_SELECT_LANGUAGE'		=> 'Valitse kieli',
	'ACP_BOARDRULES_CREATE_RULE'			=> 'Luo sääntö',
	'ACP_BOARDRULES_CREATE_RULE_EXPLAIN'	=> 'Voit luoda uuden säännön alla olevalla lomakkeella.',
	'ACP_BOARDRULES_EDIT_RULE'				=> 'Muokkaa sääntöä',
	'ACP_BOARDRULES_EDIT_RULE_EXPLAIN'		=> 'Voit muokata olemassa olevaa sääntöä alla olevalla lomakkeella.',
	'ACP_RULE_SETTINGS'						=> 'Säännön asetukset',
	'ACP_RULE_PARENT'						=> 'Ylätaso',
	'ACP_RULE_NO_PARENT'					=> 'Ei ylätasoa',
	'ACP_RULE_TITLE'						=> 'Säännön otsikko',
	'ACP_RULE_TITLE_EXPLAIN'				=> 'Vain sääntökategorioiden otsikot näkyvät sääntösivulla. Sääntöjen otsikoita käytetään myös sääntöjen erottelemiseen ylläpidon hallintapaneelissa.',
	'ACP_RULE_ANCHOR'						=> 'Säännön ankkuri',
	'ACP_RULE_ANCHOR_EXPLAIN'				=> 'Sääntöjen ankkurit ovat valinnaisia ja toimivat sääntösivun linkkien kohdeankkureina. Niiden tulisi alkaa kirjaimella ja olla käyttökelpoisia URL-osoitteissa (ei välilyöntejä tai erikoismerkkejä). Ankkureiden täytyy myös olla yksilöiviä.',
	'ACP_RULE_MESSAGE'						=> 'Säännön viesti',
	'ACP_RULE_MESSAGE_EXPLAIN'				=> 'Jokaisen säännön viesti näkyy sääntösivulla. (Kategorioiden viestit eivät näy.)',
	'ACP_RULE_MESSAGE_DISABLED'				=> 'Tämä on sääntöjä sisältävä kategoria, joten viestin muokkaaminen ei ole mahdollista.',
	'ACP_ADD_RULE'							=> 'Luo uusi sääntö',
	'ACP_DELETE_RULE_CONFIRM'				=> array(
		0 => 'Haluatko varmasti poistaa tämän säännön?',
		1 => 'Haluatko varmasti poistaa tämän sääntökategorian?<br />Varoitus: Sääntökategorian poistaminen poistaa myös kaikki sen sisältämät säännöt.',
	),
	'ACP_RULE_ADDED'						=> 'Sääntö on luotu.',
	'ACP_RULE_DELETED'						=> 'Sääntö on poistettu.',
	'ACP_RULE_EDITED'						=> 'Sääntöä on muokattu.',
	'ACP_RULE_TITLE_EMPTY'					=> 'Anna säännölle otsikko.',

	// Nested set exception messages (only appears in PHP error logging)
	// Translators: Feel free to not translate these language strings
	'RULES_NESTEDSET_LOCK_FAILED_ACQUIRE'	=> 'Board rules failed to acquire the table lock. Another process may be holding the lock. Locks are forcibly released after a timeout of 1 hour.',
	'RULES_NESTEDSET_INVALID_ITEM'			=> 'The requested rule does not exist.',
	'RULES_NESTEDSET_INVALID_PARENT'		=> 'The requested rule has no parent.',
));
