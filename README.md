See below for English description.

# Suomennos Board Rules -laajennukselle

Suomenkielinen kielipaketti phpBB:n Board Rules -laajennukselle.

## Asennus

1. Asenna Board Rules -laajennus ensin.

2. Lataa laajennuksen versiota vastaava kielipaketti alla olevasta luettelosta:

	- [2.1.1](https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb/downloads/board-rules-fi-2.1.1.zip)
	- [2.1.0](https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb/downloads/board-rules-fi-2.1.0.zip)
	- [2.0.0](https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb/downloads/board-rules-fi-2.0.0.zip)
	- [1.0.4](https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb/downloads/board-rules-fi-1.0.4.zip)
	- [1.0.3](https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb/downloads/board-rules-fi-1_0_3.7z)
	- [1.0.2](https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb/downloads/board-rules-fi-1_0_2.7z)

3. Pura paketin sisältö phpBB:n `ext/phpbb/boardrules`-hakemistoon.

# Finnish Translation for Board Rules

A Finnish language pack for the Board Rules extension for phpBB.

## Installation

1. Install the Board Rules extension first.

2. Download the language pack matching the version of the extension from the
list below:

	- [2.1.1](https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb/downloads/board-rules-fi-2.1.1.zip)
	- [2.1.0](https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb/downloads/board-rules-fi-2.1.0.zip)
	- [2.0.0](https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb/downloads/board-rules-fi-2.0.0.zip)
	- [1.0.4](https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb/downloads/board-rules-fi-1.0.4.zip)
	- [1.0.3](https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb/downloads/board-rules-fi-1_0_3.7z)
	- [1.0.2](https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb/downloads/board-rules-fi-1_0_2.7z)

3. Extract the contents of the archive into the `ext/phpbb/boardrules`
directory under your phpBB root directory.
