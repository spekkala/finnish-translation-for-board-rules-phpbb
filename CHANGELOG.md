# Change Log

## 2.1.1 (2018-01-06)

- Update translations to be compatible with Board Rules 2.1.1.

## 2.1.0 (2018-01-06)

- Update translations to be compatible with Board Rules 2.1.0.

## 2.0.0 (2017-02-26)

- Update translations to be compatible with Board Rules 2.0.0.

## 1.0.4 (2017-02-26)

- Update translations to be compatible with Board Rules 1.0.4.

## 1.0.3 (2016-03-14)

- Include extension version 1.0.3 in `README.md`.

## 1.0.2 (2015-10-06)

- Initial release.
