<?php
/**
*
* Board Rules extension for the phpBB Forum Software package.
* Finnish translation by Sami Pekkala (https://bitbucket.org/spekkala/finnish-translation-for-board-rules-phpbb)
*
* @copyright (c) 2014 phpBB Limited <https://www.phpbb.com>
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(
	'BOARDRULES_HEADER'			=> 'Keskustelupalstan säännöt',
	'BOARDRULES_EXPLAIN'		=> 'Näiden sääntöjen tarkoituksena on selventää, minkälaiset velvollisuudet koskevat kaikkia yhteisön ”%s” jäseniä. Jokainen jäsen sitoutuu noudattamaan sääntöjä, jotta keskustelupalstamme toimii ongelmitta sekä tarjoaa miellyttävän ja antoisan kokemuksen kaikille yhteisömme jäsenille ja vierailijoille.',
	'BOARDRULES_CATEGORIES'		=> 'Sääntöpykälät',
	'BOARDRULES_CATEGORY_ANCHOR'=> 'pykala-%s',
	'BOARDRULES_RULE_ANCHOR'	=> 'saanto-%s',
));
